package senac.com.br.doesade.modelo;

public class Paciente {

    private String nome ;
    private String cpf ;
    private String sangue ;
    private String telefone;



    public Paciente(String nome, String cpf, String sangue, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.sangue = sangue;
        this.telefone = telefone;

    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSangue() {
        return sangue;
    }

    public void setSangue(String sangue) {
        this.sangue = sangue;
    }

    public Paciente() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    @Override
    public String toString() {
        return  nome + "Tipo Sanguíneo: " + sangue ;
    }
}
