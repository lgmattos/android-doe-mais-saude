package senac.com.br.doesade;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import senac.com.br.doesade.modelo.Paciente;

public class SolicitarDoacaoActivity extends AppCompatActivity {
    private List<Paciente> listaPaciente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_doacao);



}
    public void onclickAP(View view){
        chamarAlerta();

    }

    private void chamarAlerta (){
        final CharSequence[] opcoes = {"Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMS();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMS(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("A+") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) A+, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }
    public void onclickAN(View view){
        chamarAlertaAN();

    }

    private void chamarAlertaAN (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSAN();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSAN(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("A-") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) A-, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
        }
        }



    }
    public void onclickBP(View view){
        chamarAlertaBP();

    }

    private void chamarAlertaBP (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSBP();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSBP(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("B+") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) B+, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }



    public void onclickBN(View view){
        chamarAlertaBN();

    }

    private void chamarAlertaBN (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSBN();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSBN(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("B-") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) B-, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }






    public void onclickABP(View view){
        chamarAlertaABP();

    }

    private void chamarAlertaABP (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSABP();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSABP(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("AB+") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) AB+, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }




    public void onclickABN(View view){
        chamarAlertaABN();

    }

    private void chamarAlertaABN (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSABN();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSABN(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("AB-") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) AB-, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }






    public void onclickOP(View view){
        chamarAlertaOP();

    }

    private void chamarAlertaOP (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSOP();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSOP(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("O+") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) O+, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }




    public void onclickON(View view){
        chamarAlertaON();

    }

    private void chamarAlertaON (){
        final CharSequence[] opcoes = { "Via SMS", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(SolicitarDoacaoActivity.this);
        builder.setTitle("Como deseja solicitar doação?");
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opcoes[i].equals("Via SMS")){
                    enviarSMSON();
                    // Toast.makeText(getApplicationContext(),"SMS enviado", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Cancelado",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public void enviarSMSON(){
        SmsManager smsManager = SmsManager.getDefault();
        listaPaciente = CadastroDoadorActivity.getListaDoador() ;
        for(Paciente p : listaPaciente){
            if (p.getSangue().equals("O-") ){

                Toast.makeText(getApplicationContext(),"Enviado",Toast.LENGTH_SHORT).show();

                smsManager.sendTextMessage(p.getTelefone(), null,"Caro doador(a) O-, solicitamos encarecidamente a sua doação no  hemocentro HEMOES, localizado na Av. Mal. Campos, 1468 - Nazareth, Vitória - ES.",null,null);
            }
        }



    }



}

