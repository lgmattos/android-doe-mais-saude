package senac.com.br.doesade;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import senac.com.br.doesade.modelo.Paciente;

public class CadastroDoadorActivity extends AppCompatActivity implements View.OnClickListener {


    Button buttonData;
    EditText editTextData;
    private int dia, mes, ano;

    private EditText editTextNome;
    private EditText editTextCPF ;
    private Spinner spinnerSangue;
    private EditText editTextTelefone;

    private Paciente paciente  ;

    private static List<Paciente> listaDoador = new ArrayList<>(  ) ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cadastro_doador );
        buttonData = (Button) findViewById( R.id.buttonData2 );
        buttonData.setOnClickListener(  this );


        editTextNome = findViewById(R.id.editTextNomeDoador);
        editTextCPF = findViewById(R.id.editTextCPFDoador);
        spinnerSangue = findViewById(R.id.spinnerTipoSangue2);
        editTextTelefone = findViewById(R.id.editTextTelefoneDoador);




        Button btCancelar = (Button) findViewById( R.id.buttonCancelar );
        btCancelar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder( CadastroDoadorActivity.this );
                alerta.setTitle( "Aviso" );
                alerta
                        .setIcon( R.mipmap.ic_aviso)
                        .setMessage( "Deseja cancelar o cadastro?" )
                        .setCancelable( false )
                        .setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent( CadastroDoadorActivity.this,MainActivity.class );
                                startActivity( intent );
                                Toast.makeText( getApplicationContext(),"Cadastro cancelado!",Toast.LENGTH_SHORT).show();
                            }
                        } )

                        .setNegativeButton( "NÃO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText( getApplicationContext(),"Continue o cadastro",Toast.LENGTH_SHORT ).show();
                            }
                        } );
                AlertDialog alertDialog = alerta.create();
                alertDialog.show();

            }
        } );


        Button btCadastrar = (Button) findViewById( R.id.buttonCadastrar );
        btCadastrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder( CadastroDoadorActivity.this );
                alerta.setTitle( "Aviso" );
                alerta
                        .setIcon( R.mipmap.ic_aviso )
                        .setMessage( "Deseja prosseguir o cadastro?" )
                        .setCancelable( false )
                        .setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent( CadastroDoadorActivity.this, MainActivity.class );
                                startActivity( intent );
                                cadastrarDoador();
                                //Toast.makeText( getApplicationContext(),"Cadastro realizado com sucesso", Toast.LENGTH_SHORT ).show();
                            }
                        } )
                        .setNegativeButton( "NÃO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText( getApplicationContext(),"Continue o cadastro",Toast.LENGTH_SHORT ).show();
                            }
                        } );
                AlertDialog alertDialog = alerta.create();
                alertDialog.show();
            }
        } );

    }

    public void onClick(View v) {
        if (v == buttonData) {
            final Calendar c = Calendar.getInstance();
            dia = c.get( Calendar.DAY_OF_MONTH );
            mes = c.get( Calendar.MONTH );
            ano = c.get( Calendar.YEAR );

            DatePickerDialog datePickerDialog = new DatePickerDialog( this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    buttonData.setText( dayOfMonth + "/" + (monthOfYear + 1) + "/" + year );

                }


            }, dia, mes, ano );
            datePickerDialog.show();
        }

    }
    public void cadastrarDoador(){
        paciente = new Paciente();

        paciente.setNome(editTextNome.getText().toString());
        paciente.setCpf(editTextCPF.getText().toString());
        paciente.setSangue(spinnerSangue.getSelectedItem().toString());
        paciente.setTelefone(editTextTelefone.getText().toString());
        listaDoador.add(paciente);

        Toast.makeText(this,"Salvo com sucesso!",Toast.LENGTH_SHORT).show();
    }
    public static List<Paciente>getListaDoador(){
        return listaDoador;
    }


}
