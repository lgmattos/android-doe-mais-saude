package senac.com.br.doesade;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import senac.com.br.doesade.modelo.Paciente;

public class ListaPacAdapter  extends BaseAdapter {

        Context context;
        List<Paciente> pacientes;

        public ListaPacAdapter(Context context, List<Paciente> pacientes) {
            this.context = context;
            this.pacientes = pacientes;
        }

        @Override
        public int getCount() {
            if (pacientes != null){
                return pacientes.size();

            }else {

                return 0;}
        }

        @Override
        public Object getItem(int position) {
            return pacientes.get( position );
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = LayoutInflater.from( context ).inflate( R.layout.modelo_lista_paciente, parent,false);

            TextView nome = v.findViewById( R.id.textViewNomeLista );
            TextView cpf = v.findViewById( R.id.textViewCPFLista );
            TextView sangue = v.findViewById( R.id.textViewSangueLista );
            TextView telefone = v.findViewById(R.id.textViewTelefoneModelo);

            Paciente p = pacientes.get( position );
            nome.setText( p.getNome() );
            cpf.setText( p.getCpf() );
            sangue.setText( p.getSangue() );
            telefone.setText(p.getTelefone());

            return v ;
        }
    }

