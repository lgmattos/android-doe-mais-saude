package senac.com.br.doesade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import senac.com.br.doesade.modelo.Paciente;

public class ListaPacienteActivity extends AppCompatActivity {

    private List<Paciente> listaPaciente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_lista_paciente );

        listaPaciente = CadastrarPacienteActivity.getListaPaciente() ;

        if(listaPaciente.isEmpty()) {
            Toast.makeText( this, "Não Existem pacientes cadastrados.", Toast.LENGTH_LONG ).show();
        }

        ListView listView = findViewById( R.id.listaPacientes ) ;

        ListaPacAdapter adapter = new ListaPacAdapter( getBaseContext(),listaPaciente );
        listView.setAdapter( adapter );







        // ArrayAdapter<Paciente> adapter = new ArrayAdapter<Paciente>(this, android.R.layout.simple_list_item_1, listaPaciente);







    }

    }

